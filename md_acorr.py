#!/usr/bin/python

# author: ypj@bnl.gov

import sys
import math
import numpy as np

ntypes_leaf = 5
c5=0.05
coeff_5Li = [(19-55*c5)/9.0,\
             (-64+640*c5)/45.0,\
             (1-64*c5)/9.0,\
             c5,\
             0.2-2*c5]
# [c1, c3, c2, c5, c4]

#----------------------------------------------------------------------
def parse(datapath,fhead,rawfiles,lat_size_t):
  
  nfiles = len(rawfiles)
  
  data = []

  errlog = open('error.log','w')

  for i in range(nfiles):
    md = {}
    wflow_time = [0]
    edensity = []
    leafs = []
    leafs_at_tau = []

    print 'import from', fhead+rawfiles[i]

    with open(datapath+'/'+ fhead + rawfiles[i],'r') as f:
      for l in f:
        fields = l.split()
        nfields = len(fields)
        
        if fields[0] == 'AlgWilsonFlow:':
          wflow_time.append(fields[3])
        
        if nfields == lat_size_t:
          edensity.append(fields)
        
        if nfields == lat_size_t+2 and int(fields[0]) == len(leafs_at_tau):
          leafs_at_tau.append(fields[2:])
          if len(leafs_at_tau) == ntypes_leaf:
            leafs.append(leafs_at_tau)
            leafs_at_tau = []
    
    wflow_nsteps = len(wflow_time)

    if wflow_nsteps == len(edensity) and wflow_nsteps == len(leafs):
      md['traj'] = int(rawfiles[i])
      md['wflow_time'] = np.array(wflow_time,dtype='float64')
      for i in range(wflow_nsteps-1):
        md['wflow_time'][i+1] += md['wflow_time'][i]
      md['energy_density'] = np.array(edensity,dtype='float64')
      md['clover_leafs'] = np.array(leafs,dtype='float64')
      data.append(md)
      print 'success to import', wflow_nsteps, 'Wilson flow steps'
    else:
      md['traj'] = int(rawfiles[i])
      md['wflow_time'] = np.array([-1.0],dtype='float64')
      md['energy_density'] = np.zeros((1,lat_size_t),dtype='float64')
      md['clover_leafs'] = np.zeros((1,5,lat_size_t),dtype='float64')
      data.append(md)
      print 'skipped since the file is corrupted:'
      print >> errlog, fhead+rawfiles[i], \
                       ', length wflow_time = ', wflow_nsteps, \
                       ', length energy_density = ', len(edensity), \
                       ', length clover_leafs = ', len(leafs)

  errlog.close()

  
  return data


#----------------------------------------------------------------------
def topologicalChargeSlice(clover):
  
  nflow = clover.shape[0]
  nt = clover.shape[2]
  
  tc = np.zeros((nflow,nt))
  for i in range(nflow):
    for j in range(nt):
      tc[i,j] = np.inner(coeff_5Li,clover[i,:,j])
      #print i, j, tc[i,j]
  
  #print clover.shape
  return tc


#----------------------------------------------------------------------
def topologicalCharge(clover):
  return np.sum(topologicalChargeSlice(clover),axis=1)


#----------------------------------------------------------------------
def resampleWilsonFlow(smpl_t,flow_t,flow_obs):

  if len(flow_t) == 1 and flow_t[0] == -1:
    smpl_obs = np.zeros((len(smpl_t)))
    return smpl_obs

  smpl_obs = np.interp(smpl_t,flow_t,flow_obs,left=np.nan,right=np.nan)
  
  for flag in zip(smpl_t,np.isnan(smpl_obs)):
    if flag[1]:
      print 'Terminated: cannot interpolate to flow time =', flag[0]
      exit(1)

  return smpl_obs


#----------------------------------------------------------------------
#----------------------------------------------------------------------

def main():

  datapath = "./wflow-rawdata/RMHMC/L32_beta6.4_K1.0_t2.0/results/3x3"
  rawfiles = []
  
  # measurement information
  # tau: molecular dynamics time

  tau_init = 20
  #tau_init = 1000
  tau_sep = 10
  ntraj = 980
  #ntraj = 400
  nt = 32
  
  # Wilson flow sampling
  flow_time = [0,1,2,3,4,5,6,7,8,9,10,15,20]
  nflow = len(flow_time)

  # MD separation sampling
  md_sep = [0,1,2,3,4,5,6,7,8,9,\
            10,15,20,25,30,35,40,45,50,55,60,\
            70,80,90,100,200,300]
  
  # read in data
  #-------------
  for i in range(ntraj):
    rawfiles.append(str(tau_init+i*tau_sep))
  
  data = parse(datapath,'wflow.',rawfiles,nt)


  # resample flow time
  #-------------------
  # t^2 E
  tsqE = np.zeros((nflow,ntraj))
  
  for s in range(ntraj):
    tsqE_raw = np.mean(data[s]['energy_density'],axis=1)\
               *np.power(data[s]['wflow_time'],2)
    tsqE[:,s] = resampleWilsonFlow(flow_time,\
                                   data[s]['wflow_time'],\
                                   tsqE_raw)
  
  # time sliced topological charge
  #topologicalCharge(data[0]['clover_leafs'])
  Qtop = np.zeros((nflow,nt,ntraj))
  
  for s in range(ntraj):
    Qtop_raw = topologicalChargeSlice(data[s]['clover_leafs'])
    for t in range(nt):
      Qtop[:,t,s] = resampleWilsonFlow(flow_time,\
                                       data[s]['wflow_time'],\
                                       Qtop_raw[:,t])


  # topological charge diffusion
  #-----------------------------
  # 'clover_leafs' indexing (flow_time,leaf,time)


  fmtstr='{0:d} {1:d} {2:d} {3:13.6e} '\
         + '{4:13.6e} {5:13.6e} {6:13.6e} {7:13.6e} '\
         + '{8:13.6e} {9:13.6e} {10:13.6e} {11:13.6e}'
  legend='flow_time  MD_sep time_sep  frequency  '\
          + 'C/C00  e(C)/C00  C  e(C)  '\
          + 'F[C]/(F[C](k=0))  e(F[C])/(F[C](k=0))  F[C]  e(F[C])'

  with open('Qtop_acorr_wflow.dat','w') as outfile:
    print >> outfile, legend

  freq = np.fft.fftfreq(nt)*2.0*math.pi
  
  for f in range(nflow):
    
    fmode = 'a'
    Qtop_acorr_norm = np.zeros((len(md_sep)))
    mode_exp_t_norm = np.zeros((len(md_sep)))

    for s in range(len(md_sep)):

      print 'processing flow time {0:d}, MD time {1:d}'.format(flow_time[f],md_sep[s])
      
      # construct correlator
      Qtop_acorr = np.zeros((nt,nt,ntraj-md_sep[s]))
      
      for t0 in range(nt):
        for s0 in range(ntraj-md_sep[s]):
          Qtop_acorr[:,t0,s0] \
          = np.roll(Qtop[f,:,md_sep[s]+s0],-t0,axis=0)*Qtop[f,t0,s0]
      
      # make jackknife samples
      Qtop_acorr_sum_t0s0 = np.sum(np.sum(Qtop_acorr,axis=2),axis=1)
      
      nsamp = nt*(ntraj-md_sep[s])
      scale_jk = (nsamp-1)/math.sqrt(float(nsamp))
      Qtop_acorr_jk = np.zeros((nt,nsamp))
      mode_exp_t_jk = np.zeros((nt,nsamp))
      
      isamp = 0
      for t0 in range(nt):
        for s0 in range(ntraj-md_sep[s]):
          Qtop_acorr_jk[:,isamp] = Qtop_acorr_sum_t0s0 - Qtop_acorr[:,t0,s0]
          Qtop_acorr_jk[:,isamp] /= float(nsamp-1)
          mode_exp_t_jk[:,isamp] = np.fft.fft(Qtop_acorr_jk[:,isamp],nt).real
          isamp += 1
      
      # save jackknife average and error
      Qtop_acorr_avg = np.mean(Qtop_acorr_jk,axis=1)
      Qtop_acorr_err = np.std(Qtop_acorr_jk,axis=1,ddof=1)*scale_jk
      Qtop_acorr_norm[s] = Qtop_acorr_sum_t0s0[0]/float(nsamp)

      mode_exp_t_avg = np.mean(mode_exp_t_jk,axis=1)
      mode_exp_t_err = np.std(mode_exp_t_jk,axis=1,ddof=1)*scale_jk
      mode_exp_t_norm[s] = np.fft.fft(Qtop_acorr_sum_t0s0,nt).real[0]/float(nsamp)

      with open('Qtop_acorr_wflow.dat',fmode) as outfile:
        for t in range(nt):
          print >> outfile, fmtstr\
              .format(flow_time[f],md_sep[s],t,freq[t],\
                      Qtop_acorr_avg[t]/Qtop_acorr_norm[0],\
                      Qtop_acorr_err[t]/abs(Qtop_acorr_norm[0]),\
                      Qtop_acorr_avg[t],\
                      Qtop_acorr_err[t],\
                      mode_exp_t_avg[t]/mode_exp_t_norm[s],\
                      mode_exp_t_err[t]/abs(mode_exp_t_norm[s]),\
                      mode_exp_t_avg[t],\
                      mode_exp_t_err[t])

      fmode = 'a'



if __name__ == '__main__':
  main()
