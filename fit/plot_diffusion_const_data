#!/bin/bash

FHEAD=meff_Qtslice_mode
DAT="${FHEAD}.tmp"

wflow=8
nt=32
max_freq=15

for n in `seq 0 ${max_freq}`
do

FILE="${FHEAD}_k${n}_wflow${wflow}.lta"

CFIT=($(awk '$1 ~ /#c_fit/ {
			  for(i=2;i<=NF;i++){ print $i; }
			}' < ${FILE}))

CERR=($(awk '$1 ~ /#c_err/ {for(i=2;i<=NF;i++)
							  { print $i; }
							}' < ${FILE}))

echo ${n} ${CFIT[1]} ${CERR[1]} >> ${DAT}

done

fig=$(basename $0)
fig=${fig#plot_}_wflow${wflow}


# draw a effective mass plot
gnuplot << EOF
set terminal epslatex size 12cm, 8cm color colortext font 10

# black / continuous line / circle with central dot
set style line 1 pt 6 ps 2.0 lt 1 lw 2.5 lc rgb '#000000'
set style line 2 pt 5 ps 0.5 lt 1 lw 2.5 lc rgb '#000000'

# crimson / continuous line
set style line 12 lt 1 lw 3.0 lc rgb '#ff0000'

# crimson / dotted line
set style line 102 lt 2 lw 2.0 lc rgb '#ff0000'

set output '${fig}.tex'
set size 1.0, 1.0
set origin 0.0, 0.0
xa = 0
xb = 4.0
set xrange [xa-0.1:xb+0.1]
set xtics 1
set mxtics 5
set format x '\${%.0f}\$'
#set xlabel '\$n\$'
set xlabel '\$4\sin^2(k/2)\$'
#set xlabel '\$k^2\$'
ya = 0
yb = 0.2
set yrange [ya:yb]
set ytics 0.1
set mytics 5
set format y '\${%.1f}\$'
set ylabel '\$D^\prime\$' rotate by 90

measure(x) = 2-2*cos(2*pi*x/${nt})
#measure(x) = (2*pi*x/${nt})**2

set key top right samplen 2.0
plot '${DAT}' using (measure(\$1)):2:3 with errorbars ls 1 notitle,\
     '${DAT}' using (measure(\$1)):2 with point ls 2 notitle
EOF

combine=combine-epslatex
#${combine} -b -f bera -s large ${fig}
${combine} -f cmbright -s normalsize ${fig}
ps2pdf ${fig}.eps
#pdfcrop --margins -3 ${fig}.pdf ${fig}.pdf
pdfcrop ${fig}.pdf ${fig}.pdf
rm -f ${fig}.eps
rm -f ${DAT}
