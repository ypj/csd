#!/bin/bash

# PURPOSE:
# generate queue file for the script run_corfit_parallel
# and summerize fit results
#
# author: ypj@bnl.gov

# configuration info
ncfg=10336
nt=11

# fit function
fitfunc=1+0.nosym
nparam=2

# data symmetrize
symflag=no

# fit range
tmin=4
tmax=10

# minimizer
minalg=gsl_deriv
maxiter=10000
#mintol=1.0e-10
mintol=1.0e-6

# covariance matrix control
covmat=full
svdmax=$[$[${tmax}-${tmin}]+1]
#svdmax=8
svdtol=1.0e-20

# error estimation
#resample=fit
resample=jk

queue="in.queue.corfit"

wflow=(8 1 2 4)
freq=($(seq 0 15))

# data path (output of coravg_src)
datapath=../../corr/
data=data

if [ -e data ]; then
  rm -f data
fi
ln -sf ${datapath} data

#---------------------------------------------------------------------
if [ "$1" == "--build" ]; then

if [ -f ${queue} ]; then
  echo "Terminated: ${queue} exists."
  exit 1
fi

prior=()
width=()
for f in ${wflow[@]}
do
  for k in ${freq[@]}
  do
    file="${data}/Qtslice_mode_k${k}_wflow${f}.lta"
    args=(-f ${file})
    args+=(--samp ${ncfg})
    args+=(--dim ${nt}) 
    args+=(--symm ${symflag})
    args+=(--func ${fitfunc})
    for (( i=0; i<${#prior[@]}; i++ )); do
      args+=(-c ${prior[$i]})
    done
    for (( i=0; i<${#width[@]}; i++ )); do
      args+=(-w ${width[$i]})
    done
    args+=(-l ${tmin})
    args+=(-u ${tmax})
    args+=(--cov ${covmat}) 
    args+=(--svdmax ${svdmax})
    args+=(--svdinv ${svdtol})
    args+=(--min ${minalg})
    args+=(--iter_fdf ${maxiter})
    args+=(--mintol ${mintol})
    args+=(--err ${resample})
    args+=(--dtype "rawjk")
    args+=(--meff)
    echo  ${args[*]} >> ${queue}
  done
done

##---------------------------------------------------------------------
#elif [ "$1" == "--check" ]; then
#
#for kap in ${kappa[@]}
#do
#  kap=0.${kap}
#  dir="./log"
#
#  for cha in pi_${smear}_m0.${ml} rho_${smear}_m0.${ml}
#  do
#
#    logfile="${dir}/check_collect_${cha}_k${kap}.log"
#    exec 1>${logfile}
#    exec 2>&1
#
#    nfile=$(ls -1 fit_corr_${cha}_k${kap}_p* | wc -l)
#    echo "channel=${cha}, kappa=${kap}, maxiter=${maxiter}, "\
#         "finished fit = ${nfile}"
#
#    for (( ip=0; ip < ${#xyz[@]}; ip++ ))
#    do
#      file="${cha}_k${kap}_p${xyz[${ip}]}.log"
#      if [ -f ${dir}/${file} ] ; then
#        cnt=$(grep -e -${maxiter}\ \(nt\) ${dir}/${file} | wc -l)
#        echo "${xyz[${ip}]}: unconverged sample fits = ${cnt}"
#        grep -e iteration ${dir}/${file} | head -2
#      fi
#    done
#    echo "---------------------------------------------------------------"
#
#    for (( ip=0; ip < ${#xyz[@]}; ip++ ))
#    do
#      file="${cha}_k${kap}_p${xyz[${ip}]}.log"
#      if [ -f ${dir}/${file} ] ; then
#        echo ${xyz[${ip}]}: $(grep -e Q\ \ \ \ \= ${dir}/${file})
#      fi
#    done
#    echo "---------------------------------------------------------------"
#    
#    
#    for (( ip=0; ip < ${#xyz[@]}; ip++ ))
#    do
#      file="${cha}_k${kap}_p${xyz[${ip}]}.log"
#      if [ -f ${dir}/${file} ] ; then
#        echo ${xyz[${ip}]}: $(grep -e CDF\ \ \= ${dir}/${file})
#      fi
#    done
#    echo "---------------------------------------------------------------"
#    
#    for (( ic=0; ic < ${nparam}; ic++ ))
#    do
#      for (( ip=0; ip < ${#xyz[@]}; ip++ ))
#      do
#        file="${cha}_k${kap}_p${xyz[${ip}]}.log"
#        if [ -f ${dir}/${file} ] ; then
#        echo ${xyz[${ip}]}: \
#          $(tail -20 ${dir}/${file} | grep -e c\\[${ic}\\])
#        fi
#      done
#      echo "---------------------------------------------------------------"
#    done
#
#  done
#done
#
#---------------------------------------------------------------------
elif [ "$1" == "--run" ]; then

  ./run_corfit_parallel ${queue}

##---------------------------------------------------------------------
#elif [ "$1" == "--merge" ]; then
#    
#  p0_list=($(find ./plots -name *p000.pdf -exec basename \{} .pdf \; | sort -n))
#
#  cd plots
#
#  for zero in ${p0_list[@]}
#  do
#    channel=$(echo ${zero} | sed 's/_p000//g')
#    echo "merge ${channel}"
#    fig_list=("${zero}.pdf")
#    for (( i=1; i<${#xyz[@]}; i++ )); do
#      p_i=$(echo ${zero} | sed 's/_p000/_p'${xyz[${i}]}'/g')
#      fig_list+=("${p_i}.pdf")
#    done
#
#    pdftk ${fig_list[@]} cat output merge_${channel}.pdf 
#  done
#
#    rm -f merge_spec.pdf
#    echo "merge all channels"
#    pdftk merge*.pdf cat output merge_spec.pdf
#
#  cd -
#		
#---------------------------------------------------------------------
else

  echo "option: --build, --run, --check, --merge"

fi
