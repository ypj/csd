#!/usr/bin/python

# author: ypj@bnl.gov

import sys, os
import math
import numpy as np
import h5py 

#----------------------------------------------------------------------
# ref. code:
# https://www.darkcoding.net/software/pretty-command-
# line-console-output-on-unix-in-python-and-go-lang/
import fcntl,termios,struct,time

COLS = struct.unpack('hh',  fcntl.ioctl(sys.stdout, termios.TIOCGWINSZ, '1234'))[1]

def refreshLine(fmt):
    
  sys.stdout.write(fmt)
  sys.stdout.flush()


def progress(current, total):
  prefix = '%3d / %3d' % (current, total)
  bar_start = ' ['
  bar_end = '] '
  
  bar_size = COLS - len(prefix + bar_start + bar_end)
  amount = int(current / (total / float(bar_size)))
  remain = bar_size - amount
  
  bar = '=' * amount + ' ' * remain
  refreshLine(prefix + bar_start + bar + bar_end + '\r')
 

def progress_quit():
  print('')


# usage
#NUM = 100
#for i in range(NUM + 1):
#    sys.stdout.write(progress(i, NUM) + '\r')
#    sys.stdout.flush()
#    time.sleep(0.05)
#print('\n')


#----------------------------------------------------------------------
ntypes_leaf = 5
c5=0.05
coeff_5Li = [(19-55*c5)/9.0,\
             (-64+640*c5)/45.0,\
             (1-64*c5)/9.0,\
             c5,\
             0.2-2*c5]
# [c1, c3, c2, c5, c4]


#----------------------------------------------------------------------
def topologicalChargeSlice(clover):
  
  nflow = clover.shape[0]
  nt = clover.shape[2]
  
  tc = np.zeros((nflow,nt))
  for i in range(nflow):
    for j in range(nt):
      tc[i,j] = np.inner(coeff_5Li,clover[i,:,j])
      #print i, j, tc[i,j]
  
  #print clover.shape
  return tc


#----------------------------------------------------------------------
def topologicalCharge(clover):
  return np.sum(topologicalChargeSlice(clover),axis=1)


#----------------------------------------------------------------------
def resampleWilsonFlow(smpl_t,flow_t,flow_obs):

  if len(flow_t) == 1 and flow_t[0] == -1:
    smpl_obs = np.zeros((len(smpl_t)))
    return smpl_obs

  smpl_obs = np.interp(smpl_t,flow_t,flow_obs,left=np.nan,right=np.nan)
  
  for flag in zip(smpl_t,np.isnan(smpl_obs)):
    if flag[1]:
      print 'Terminated: cannot interpolate to flow time =', flag[0]
      exit(1)

  return smpl_obs


#----------------------------------------------------------------------
def listTrajectories(hfname):

  print 'decode file info:', hfname

  hf = h5py.File(hfname,'r')
  
  traj = []
  for k in hf.keys():
    traj.append(int(k))
  
  hf.close()
  
  #traj.sort()
  
  traj_n = len(traj)
  #traj_i = traj[0]
  traj_i = min(traj)
  #traj_f = traj[traj_n-1]
  traj_f = max(traj)
  traj_s = (traj_f - traj_i)/(traj_n-1)
   
  return traj_n, traj_i, traj_s, traj_f


#----------------------------------------------------------------------
def sampleMarkovChain(ntraj,tau_init,tau_sep,tau_last,sample_params):
  
  print 'original (ntraj, tau_init, tau_sep, tau_last) =',\
        ntraj, tau_init, tau_sep, tau_last
  
  assert (sample_params['trim_head'] >= 0),'must be non-negative'
  assert (sample_params['trim_tail'] >= 0),'must be non-negative'
  assert (sample_params['sample_rate'] > 0),'must be positive'

  tau_init += tau_sep*sample_params['trim_head']
  tau_last -= tau_sep*sample_params['trim_tail'] 
  tau_sep *= sample_params['sample_rate']
  ntraj = (tau_last-tau_init)/tau_sep + 1
  tau_last = tau_init + tau_sep*(ntraj-1)
  
  assert (tau_init<tau_last),'unacceptable'
 
  print 'resample (ntraj, tau_init, tau_sep, tau_last) =',\
         ntraj, tau_init, tau_sep, tau_last
  
  return ntraj, tau_init, tau_sep, tau_last


#----------------------------------------------------------------------
def reloadFromHDF5(hfname,dkey,ntraj,tau_init,tau_sep):
  
  print 'reload', dkey, 'from', hfname

  data = []
  hf = h5py.File(hfname,'r')
  
  tau = tau_init
  for i in range(ntraj):
    progress(i+1,ntraj)
    g = hf[str(tau)]
    data.append(g[dkey][:])
    tau += tau_sep
  progress_quit()
  
  hf.close()

  return data


#----------------------------------------------------------------------
#----------------------------------------------------------------------

def main():
  
  # Wilson flow sampling
  #flow_time = [0,1,2,3,4,5,6,7,8,9,10,15,20]
  flow_time = [0,1,2,4,8]
  #flow_time = [8]
  
  nflow = len(flow_time)

  # MD separation sampling
  #md_sep_max = 100
  #md_sep = [0,1,2,3,4,5,6,7,8,9,\
  #          10,15,20,25,30,35,40,45,50,55,60,\
  #          70,80,90,md_sep_max]
  md_sep_max = 20
  md_sep = [0,1,2,3,4,5,6,7,8,9,\
            10,15,md_sep_max]
  #md_sep_max = 2
  #md_sep = [md_sep_max]

  bin_size = 50

  # measurement information
  # tau: molecular dynamics time
  ntraj, tau_init, tau_sep, tau_last = listTrajectories('wflow.h5')

  sample_params = {'trim_head':50, 'trim_tail':0, 'sample_rate':1}
  ntraj, tau_init, tau_sep, tau_last = \
  sampleMarkovChain(ntraj, tau_init, tau_sep, tau_last, sample_params)

  ntraj_eff = ((ntraj-md_sep_max)/bin_size)*bin_size
  assert (ntraj_eff >= 1),'Too large bin_size or md_sep_max.'
  
  param_str = 'n'+str(ntraj)+'_i'+str(tau_init)+'_s'+str(tau_sep)+'_b'+str(bin_size)
  
  if not os.path.isdir(param_str): os.mkdir(param_str)

  # data reload
  #------------
  wflow_time = reloadFromHDF5('wflow.h5','wflow_time',\
                                ntraj,tau_init,tau_sep)
  
  clover_leafs = reloadFromHDF5('wflow.h5','clover_leafs',\
                                ntraj,tau_init,tau_sep)
  # 'clover_leafs' indexing (flow_time,leaf,time)
  
  nt = clover_leafs[0].shape[2]


  # resample flow time
  #-------------------
  print 'resample flow time'
  Qtop = np.zeros((nflow,nt,ntraj))
  
  for s in range(ntraj):
    progress(s+1,ntraj)
    Qtop_raw = topologicalChargeSlice(clover_leafs[s])
    for t in range(nt):
      Qtop[:,t,s] = resampleWilsonFlow(flow_time,\
                                       wflow_time[s],\
                                       Qtop_raw[:,t])
  progress_quit()


  fmtstr='{0:d} {1:d} {2:d} {3:13.6e} '\
         + '{4:13.6e} {5:13.6e} {6:13.6e} {7:13.6e} '\
         + '{8:13.6e} {9:13.6e} {10:13.6e} {11:13.6e}'
  legend='flow_time  MD_sep time_sep  frequency  '\
          + 'C/C00  e(C)/C00  C  e(C)  '\
          + 'F[C]/(F[C](k=0))  e(F[C])/(F[C](k=0))  F[C]  e(F[C])'

  with open(param_str+'/Qtop_acorr_wflow.dat','w') as outfile:
    print >> outfile, '#', param_str
    print >> outfile, legend

  freq = np.fft.fftfreq(nt)*2.0*math.pi
  
  hf = h5py.File(param_str+'/Qtslice_mode.h5','w')
  
  for f in range(nflow):
    
    fmode = 'a'
    Qtop_acorr_norm = np.zeros((len(md_sep)))
    mode_exp_t_norm = np.zeros((len(md_sep)))

    for s in range(len(md_sep)):

      #print 'processing flow time {0:d}, MD time {1:d}'.format(flow_time[f],md_sep[s])
      refreshLine('processing flow time %d, MD time %d\r' % (flow_time[f],md_sep[s]))
      
      # construct correlator
      Qtop_acorr = np.zeros((nt,nt,ntraj_eff))
      
      for t0 in range(nt):
        for s0 in range(ntraj_eff):
          Qtop_acorr[:,t0,s0] \
          = np.roll(Qtop[f,:,md_sep[s]+s0],-t0,axis=0)*Qtop[f,t0,s0]
      
      # make jackknife samples
      Qtop_acorr_sum_t0s0 = np.sum(np.sum(Qtop_acorr,axis=2),axis=1)
      
      nsamp = nt*(ntraj_eff/bin_size)
      scale_jk = (nsamp-1)/math.sqrt(float(nsamp))
      Qtop_acorr_jk = np.zeros((nt,nsamp))
      mode_exp_t_jk = np.zeros((nt,nsamp))
      
      isamp = 0
      for t0 in range(nt):
        for s0_bin in range(0,ntraj_eff,bin_size):
          #Qtop_acorr_jk[:,isamp] = Qtop_acorr_sum_t0s0 - Qtop_acorr[:,t0,s0]
          bin_sum = np.sum(Qtop_acorr[:,t0,s0_bin:s0_bin+bin_size],axis=1)
          Qtop_acorr_jk[:,isamp] = Qtop_acorr_sum_t0s0 - bin_sum
          Qtop_acorr_jk[:,isamp] /= float((nsamp-1)*bin_size)
          mode_exp_t_jk[:,isamp] = np.fft.fft(Qtop_acorr_jk[:,isamp],nt).real
          isamp += 1
      
      # save jackknife sample
      hg = hf.create_group('wflow'+str(flow_time[f])+'/tau'+str(md_sep[s]))
      for t in range(nt):
        hg.create_dataset(str(t),data=mode_exp_t_jk[t,:],dtype='f8')

      # save jackknife average and error
      Qtop_acorr_avg = np.mean(Qtop_acorr_jk,axis=1)
      Qtop_acorr_err = np.std(Qtop_acorr_jk,axis=1,ddof=1)*scale_jk
      Qtop_acorr_norm[s] = Qtop_acorr_sum_t0s0[0]/float(nsamp*bin_size)

      mode_exp_t_avg = np.mean(mode_exp_t_jk,axis=1)
      mode_exp_t_err = np.std(mode_exp_t_jk,axis=1,ddof=1)*scale_jk
      mode_exp_t_norm[s] = np.fft.fft(Qtop_acorr_sum_t0s0,nt).real[0]/float(nsamp*bin_size)

      with open(param_str+'/Qtop_acorr_wflow.dat',fmode) as outfile:
        for t in range(nt):
          print >> outfile, fmtstr\
              .format(flow_time[f],md_sep[s],t,freq[t],\
                      Qtop_acorr_avg[t]/Qtop_acorr_norm[0],\
                      Qtop_acorr_err[t]/abs(Qtop_acorr_norm[0]),\
                      Qtop_acorr_avg[t],\
                      Qtop_acorr_err[t],\
                      mode_exp_t_avg[t]/mode_exp_t_norm[s],\
                      mode_exp_t_err[t]/abs(mode_exp_t_norm[s]),\
                      mode_exp_t_avg[t],\
                      mode_exp_t_err[t])

      fmode = 'a'
  
  hf.close()



if __name__ == '__main__':
  main()
