#!/bin/bash

md_samples=25
md_max=24
scale_tau=1

dat=Qtop_acorr_wflow.dat
#t=(0 3 6 9 12 15)
t=(0 1 2 3 4 5)

#for i in 0 1 2 3 4 5 10 20
for i in 1 2 4 8
do
fig=$(basename $0)
fig=${fig#plot_}_wflow${i}
subdat=tmp-acorr

if [ -f ${subdat} ]; then
  echo "exit: need to remove ${subdat}"
  exit 10
fi

for j in ${t[@]}
do
  awk '{if ($1 == '${i}' && $3 == '${j}') {print $2, $5, $6}}' ${dat} \
  >> ${subdat}
done

gnuplot << EOF
set terminal epslatex size 12cm, 9cm color colortext font 10
set output '${fig}.tex'

#stats '${dat}'

#set offsets 0,0,0,0.1

set xlabel '\$\tau\$'
set xrange [-0.5:${md_max}+0.5]
#set xrange [-0.5:$[${md_max}*${scale_tau}]+0.5]
#set xtics 5
set mxtics 5
set ylabel '\$\ln [C(t,\tau)/C(0,0)]\$'
set yrange [-2.2:0.2]
set ytics 0.5
set mytics 5
#set key top right horizontal width 1.0 spacing 1.5 samplen 2.0 box
set key bottom left horizontal maxcol 3 width 1.5 spacing 1.2 samplen 2.0

lnum(x) = ${md_samples}*x
rnum(x) = ${md_samples}*(x+1)-1

set label '\$\text{flow time}=${i}\$' at graph 0.05,0.96

true_x(x) = ${scale_tau}*x
f(x) = (x<=0) ? 1/0 : log(x)
ef(x,err) = (x<=0) ? 1/0 : err/x

plot \
  '${subdat}' every ::(lnum(0))::(rnum(0)) using (true_x(\$1)):(f(\$2)):(ef(\$2,\$3)) with errorbar pt 1 ps 1.5 lt 1 lw 2.0 lc rgb '#0000ff' title '\$t=${t[0]}\$',\
  '${subdat}' every ::(lnum(1))::(rnum(1)) using (true_x(\$1)):(f(\$2)):(ef(\$2,\$3)) with errorbar pt 6 ps 2.0 lt 1 lw 2.5 lc rgb '#0000ff' title '\$t=${t[1]}\$',\
  '${subdat}' every ::(lnum(2))::(rnum(2)) using (true_x(\$1)):(f(\$2)):(ef(\$2,\$3)) with errorbar pt 2 ps 2.0 lt 1 lw 2.5 lc rgb '#000000' title '\$t=${t[2]}\$',\
  '${subdat}' every ::(lnum(3))::(rnum(3)) using (true_x(\$1)):(f(\$2)):(ef(\$2,\$3)) with errorbar pt 4 ps 2.0 lt 1 lw 2.5 lc rgb '#00cc00' title '\$t=${t[3]}\$',\
  '${subdat}' every ::(lnum(4))::(rnum(4)) using (true_x(\$1)):(f(\$2)):(ef(\$2,\$3)) with errorbar pt 12 ps 2.0 lt 1 lw 2.5 lc rgb '#31a8bd' title '\$t=${t[4]}\$',\
  '${subdat}' every ::(lnum(5))::(rnum(5)) using (true_x(\$1)):(f(\$2)):(ef(\$2,\$3)) with errorbar pt 8 ps 2.0 lt 1 lw 2.5 lc rgb '#ff0000' title '\$t=${t[5]}\$',\
  '${subdat}' every ::(lnum(0))::(rnum(0)) using (true_x(\$1)):(f(\$2)-1.0) with lines lt 1 lw 2.0 lc rgb '#0000ff' notitle
EOF

combine=combine-epslatex
#${combine} -b -f bera -s large ${fig}
${combine} -f cmbright -s normalsize ${fig}
ps2pdf ${fig}.eps
#pdfcrop --margins -3 ${fig}.pdf ${fig}.pdf
pdfcrop ${fig}.pdf ${fig}.pdf
rm -f ${fig}.eps 
rm -f ${subdat}

done
