#!/usr/bin/python

# author: ypj@bnl.gov

import sys
import math
import os
import numpy as np
import h5py 

#----------------------------------------------------------------------
def writeCorrelator(x,y,fname,fmt):
  
  assert (len(x) == y.shape[0]),'Domain length does not match.'
  
  if not os.path.isdir('corr'): os.mkdir('corr')

  if fmt == 'lta':
    with open('./corr/'+fname+'.'+fmt,'w') as fp:
      print >> fp,\
          '# correlator: {0:s}'.format(os.getcwd())
      for isamp in range(y.shape[1]):
        print >> fp,\
            '# configuration {0:d}'.format(isamp)
        for j in range(len(x)): 
          print >> fp,\
              '{0:d}\t{1:13.6e}\t{2:13.6e}'.format(x[j],y[j,isamp],0.0)

  else: 
    print 'No support for format =', fmt
    exit(1)
   

#----------------------------------------------------------------------
#----------------------------------------------------------------------

def main():
  
  # Wilson flow sampling
  flow_time = [0,1,2,4,8]
  
  nflow = len(flow_time)

  # MD separation sampling
  md_sep = [0,1,2,3,4,5,6,7,8,9,10]

  n_freq = 16
  
  # Currently, fitter is external

  # convert format h5 to lta
  #-------------------------
  hf = h5py.File('Qtslice_mode.h5','r')

  ds = hf['wflow'+str(flow_time[0])+'/tau'+str(md_sep[0])+'/0']
  nsamp = ds.shape[0]
  print 'number of samples:', nsamp
   
  for f in flow_time:
    for k in range(n_freq):
      corr_jk = np.zeros((len(md_sep),nsamp))
      for s in md_sep:
        ds = hf['wflow'+str(f)+'/tau'+str(s)][str(k)]
        assert (ds.shape[0]==nsamp),'Sample size does not match.'
        corr_jk[s,:] = ds[:]
      fname = 'Qtslice_mode_k'+str(k)+'_wflow'+str(f)
      writeCorrelator(md_sep,corr_jk,fname,'lta')
  
  hf.close()
  


if __name__ == '__main__':
  main()
