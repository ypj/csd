#!/bin/bash

nt=32
maxfreq=$[${nt}/2]

dat=Qtop_acorr_wflow.dat
freq=(0)

#for i in 0 1 2 3 4 5 10 20
for i in 0 1 2 3 4 5 10 15 20
do
fig=$(basename $0)
fig=${fig#plot_}_wflow${i}
subdat=tmp-acorr-mode

if [ -f ${subdat} ]; then
  echo "exit: need to remove ${subdat}"
  exit 10
fi

for j in ${freq[@]}
do
  awk '{if ($1 == '${i}' && $3 == '${j}') {print $2, $9, $10}}' ${dat} \
  >> ${subdat}
done

gnuplot << EOF
set terminal epslatex size 12cm, 9cm color colortext font 10
set output '${fig}.tex'

#stats '${dat}'

#set offsets 0,0,0,0.1
set size 1.0,0.75

set xlabel '\$\tau\$'
#set xrange [-0.5:${maxfreq}]
set xrange [-0.01:40]
#set xtics 5
set mxtics 5
set ylabel '\$\ln\widehat{C}(0,\tau)\$'
#set yrange [-1.35:1.15]
#set ytics -1,0.2,1
set mytics 5
#set key top right horizontal width 1.0 spacing 1.5 samplen 2.0 box
#set key bottom left horizontal maxcol 3 width 1.5 spacing 1.2 samplen 2.0
#set key horizontal maxcol 3 width 1.5 spacing 1.2 samplen 2.0 box \
#        at graph 0.7, graph -0.25
unset key

set label '\$\text{flow time}=${i}\$' at graph 0.65,0.94
#set arrow from graph 0, first 0 to graph 1, first 0 nohead 

lnum(x) = ${nt}*x
rnum(x) = ${nt}*(x+1)-1
measure(x) = 2-2*cos(x)

f(x) = (x<=0) ? 1/0 : log(x)
ef(x,err) = (x<=0) ? 1/0 : err/abs(x)

plot \
  '${subdat}' using 1:(f(\$2)):(ef(\$2,\$3)) with errorbar pt 4 ps 1.5 lw 2.0 lc rgb '#0000ff' notitle
EOF

combine=combine-epslatex
#${combine} -b -f bera -s large ${fig}
${combine} -f cmbright -s normalsize ${fig}
ps2pdf ${fig}.eps
#pdfcrop --margins -3 ${fig}.pdf ${fig}.pdf
pdfcrop ${fig}.pdf ${fig}.pdf
rm -f ${fig}.eps
rm -f ${subdat}

done
