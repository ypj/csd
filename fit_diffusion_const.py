#!/usr/bin/python

# author: ypj@bnl.gov

import sys
import math
import os
import numpy as np
from scipy.stats import chi2

#----------------------------------------------------------------------
def jkfilename(freq,flow):

  jkfile_head = 'jk_fit_Qtslice_mode'
  return jkfile_head+'_k'+str(freq)+'_wflow'+str(flow)+'.lta'


#----------------------------------------------------------------------
# matmul is not defined in the Numpy version < 1.10.0
def MATMUL(a,b):
   return np.dot(a,b)


#----------------------------------------------------------------------
# linear chisquared minimize fit
#
# Given fit function f_i = c_k fb_ik, 
# where c = fit parameter, fb = basis function
#
# Then, solve A_kl c_l = b_k,
# where A_kl = fb_ik s_ij fb_jl, b_k = y_i s_ij fb_jk,
# s = inverse covariance matrix, y = data
def linearFit(c_fit,fb,y,f_fit,fp_fit):

  nparam = c_fit.shape[0]
  npt = y.shape[0]
  nsamp = y.shape[1]

  jk_norm = math.sqrt((nsamp-1)/float(nsamp))
  avg = np.mean(y,axis=1)
  
  fcov = np.zeros((npt,npt))
  for i in range(npt):
    for j in range(npt):
      tmp = ( y[i,:] - avg[i] )*( y[j,:] - avg[j] )
      fcov[i,j] = np.sum(tmp)
  fcov = (fcov + fcov.transpose())*0.5*jk_norm**2

  s = np.linalg.pinv(fcov)
  s = (s+s.transpose())*0.5

  # matA[k,l] = fb[iq,k] * s[iq,jq] * fb[jq,l]
  matA = MATMUL(fb[:,:nparam].transpose(),MATMUL(s,fb[:,:nparam]))
  
  invA = np.linalg.pinv(matA)
  invA = (invA+invA.transpose())*0.5

  chisq = np.zeros((nsamp))
  zerodof = 0.0
  dof = npt - nparam
  if dof == 0.0:
    zerodof = -1.0
  
  for isamp in range(nsamp):
    # vecb[k] = y[iq,isamp] * s[iq,jq] * fb[jq,k]
    vecb = MATMUL(MATMUL(s,fb[:,:nparam]).transpose(),y[:,isamp])
    # c_fit[k,isamp] = invA[k,l]*vecb[l]
    c_fit[:,isamp] = MATMUL(invA,vecb)
    # f_fit[iq,isamp] = c_fit[k,isamp]*fb[iq,k]
    f_fit[:,isamp] = MATMUL(fb[:,:nparam],c_fit[:,isamp])
    # chisq[isamp] = 
    # (f_fit[iq,isamp]-y[iq,isamp])*s[iq,jq]*(f_fit[jq,isamp]-y[jq,isamp])
    delta = f_fit[:,isamp]-y[:,isamp]
    chisq[isamp] = np.dot(delta,MATMUL(s,delta))
  
  chisq_mean = np.mean(chisq)
  chisq_err = np.std(chisq,ddof=1)
  
  p_mean = 0
  p_err = 0

  if dof > 0:
    qfactor = chi2.cdf(chisq,dof)
    p_mean = 1.0 - np.mean(qfactor)
    p_err = np.std(qfactor,ddof=1)

  print >> fp_fit, '#chisq / dof =', \
        "{0:.3e}".format(chisq_mean), '/', dof, '=', \
        "{0:.3e}".format(chisq_mean/float(dof+zerodof)), \
        "{0:.3e}".format(chisq_err/float(dof+zerodof))
  print >> fp_fit, '#p-value = {0:.3e} {1:.3e}'.format(p_mean,p_err)


#----------------------------------------------------------------------
def diffusionFitLinear(fr,y_jk,nt,fp_fit):

  npt = fr[1] - fr[0] + 1
  nsamp = y_jk.shape[1]
  ndat = y_jk.shape[0]
  nparam = 2
  
  # set basis for fit
  fb = np.zeros((npt,nparam))
  
  for n in range(npt):
    fb[n,0] = 1.0
    fb[n,1] = 4.0*math.pow(math.sin((fr[0]+n)*math.pi/float(nt)),2)
  
  # set basis for plot
  x_plot = np.zeros((ndat))
  for i in range(ndat):
    x_plot[i] = i
  
  pfb = np.zeros((ndat,nparam))
  
  for n in range(ndat):
    pfb[n,0] = 1.0
    pfb[n,1] = 4.0*math.pow(math.sin(x_plot[n]*math.pi/float(nt)),2)
  
  # do a linear fit
  c_fit = np.zeros((nparam,nsamp))
  f_fit = np.zeros((npt,nsamp))

  linearFit(c_fit,fb,y_jk[fr[0]:fr[1]+1,:],f_fit,fp_fit)

  jk_err_norm = (nsamp-1)/math.sqrt(float(nsamp))
  c_fit_mean = np.mean(c_fit,axis=1)
  c_fit_err = np.std(c_fit,axis=1,ddof=1)*jk_err_norm
  c_fit_corr = np.corrcoef(c_fit,ddof=1)
  
  # evaluate fit function for plot
  f_plot = np.zeros((ndat,nsamp))
  for isamp in range(nsamp):
    # f_plot[i,isamp] = c_fit[k,isamp]*fb[i,k]
    f_plot[:,isamp] = MATMUL(pfb[:,:nparam],c_fit[:,isamp])
  
  f_plot_mean = np.mean(f_plot,axis=1)
  f_plot_err = np.std(f_plot,axis=1,ddof=1)*jk_err_norm
  
  y_mean = np.mean(y_jk,axis=1)
  y_err = np.std(y_jk,axis=1,ddof=1)*jk_err_norm
  
  # write the fit results
  print >> fp_fit, '#c_fit',
  for i in range(nparam):
    print >> fp_fit, '{0:13.6e}'.format(c_fit_mean[i]),
  print >> fp_fit, ''

  print >> fp_fit, '#c_err',
  for i in range(nparam):
    print >> fp_fit, '{0:13.6e}'.format(c_fit_err[i]),
  print >> fp_fit, ''
  
  for i in range(nparam):
    print >> fp_fit, '#c_corr',
    for j in range(nparam):
      print >> fp_fit, '{0:13.6e}'.format(c_fit_corr[i,j]),
    print >> fp_fit, ''
  
  for i in range(ndat):
    print >> fp_fit, '{0:d} {1:13.6e} {2:13.6e} {3:13.6e} {4:13.6e}'\
        .format(i,y_mean[i],y_err[i],f_plot_mean[i],f_plot_err[i])


#----------------------------------------------------------------------
#----------------------------------------------------------------------

def main():
  
  flow_time = int(sys.argv[1])
  nt = int(sys.argv[2])
  fr_l = int(sys.argv[3])
  fr_u = int(sys.argv[4])

  nfreq = nt/2

  # read correlator fit
  jkdata = np.genfromtxt(jkfilename(0,flow_time))[1:,1:]
  (nsamp,nparam) = jkdata.shape
  
  jkdata = np.zeros((nfreq,nsamp,nparam))

  for k in range(nfreq):
    jkdata[k,:,:] = np.genfromtxt(jkfilename(k,flow_time))[1:,1:]
  
  # fit to linear model
  outfile = 'fit_diffusion_linear'\
            +'_l'+str(fr_l)+'_u'+str(fr_u)+'_wflow'+str(flow_time)+'.dat'

  with open(outfile,'w') as fp_fit:
    
    print >> fp_fit, '#nt {0:d}'.format(nt)
    print >> fp_fit, '#flow_time {0:d}'.format(flow_time)
    print >> fp_fit, '#fit_range {0:d} {1:d}'.format(fr_l,fr_u)

    diffusionFitLinear([fr_l,fr_u],jkdata[:,:,1],nt,fp_fit)



if __name__ == '__main__':
  main()
