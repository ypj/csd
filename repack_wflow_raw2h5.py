#!/usr/bin/python

# author: ypj@bnl.gov

import sys
import math
import numpy as np
import h5py

ntypes_leaf = 5

#----------------------------------------------------------------------
def parse(datapath,fhead,rawfiles,lat_size_t):
  
  nfiles = len(rawfiles)
  
  data = []

  errlog = open('error.log','w')

  for i in range(nfiles):
    md = {}
    wflow_time = [0]
    edensity = []
    leafs = []
    leafs_at_tau = []

    print 'import from', fhead+rawfiles[i]

    with open(datapath+'/'+ fhead + rawfiles[i],'r') as f:
      for l in f:
        fields = l.split()
        nfields = len(fields)
        
        if fields[0] == 'AlgWilsonFlow:':
          wflow_time.append(fields[3])
        
        if nfields == lat_size_t:
          edensity.append(fields)
        
        if nfields == lat_size_t+2 and int(fields[0]) == len(leafs_at_tau):
          leafs_at_tau.append(fields[2:])
          if len(leafs_at_tau) == ntypes_leaf:
            leafs.append(leafs_at_tau)
            leafs_at_tau = []
    
    wflow_nsteps = len(wflow_time)

    if wflow_nsteps == len(edensity) and wflow_nsteps == len(leafs):
      md['traj'] = int(rawfiles[i])
      md['wflow_time'] = np.array(wflow_time,dtype='float64')
      for i in range(wflow_nsteps-1):
        md['wflow_time'][i+1] += md['wflow_time'][i]
      md['energy_density'] = np.array(edensity,dtype='float64')
      md['clover_leafs'] = np.array(leafs,dtype='float64')
      data.append(md)
      print 'success to import', wflow_nsteps, 'Wilson flow steps'
    else:
      md['traj'] = int(rawfiles[i])
      md['wflow_time'] = np.array([-1.0],dtype='float64')
      md['energy_density'] = np.zeros((1,lat_size_t),dtype='float64')
      md['clover_leafs'] = np.zeros((1,5,lat_size_t),dtype='float64')
      data.append(md)
      print 'skipped since the file is corrupted:'
      print >> errlog, fhead+rawfiles[i], \
                       ', length wflow_time = ', wflow_nsteps, \
                       ', length energy_density = ', len(edensity), \
                       ', length clover_leafs = ', len(leafs)

  errlog.close()

  
  return data


#----------------------------------------------------------------------
#----------------------------------------------------------------------

def main():

  datapath = "./wflow-rawdata"
  rawfiles = []
  
  # measurement information
  # tau: molecular dynamics time
  tau_init = 100
  tau_sep = 1
  ntraj = 10000
  nt = 32
  
  # read in data
  #-------------
  for i in range(ntraj):
    rawfiles.append(str(tau_init+i*tau_sep))
  
  data = parse(datapath,'wflow.',rawfiles,nt)
  
  # save to hdf5 file
  #------------------
  with h5py.File('wflow.h5','w') as hf:
    for i in range(len(data)):
      g = hf.create_group(str(data[i]['traj']))
      for dkey in ['wflow_time','energy_density','clover_leafs']:
        g.create_dataset(dkey,data=data[i][dkey],dtype='f8')
  
  # data reload example
  #--------------------
  #data_reload = []
  #dkey_reload = 'clover_leafs'
  #hf = h5py.File('wflow.h5','r')
  #tau = tau_init
  #for i in range(ntraj):
  #  g = hf[str(tau)]
  #  data_reload.append(g[dkey_reload][:])
  #  tau += tau_sep
  #hf.close()
  #
  #print data_reload



if __name__ == '__main__':
  main()
