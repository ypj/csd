#!/bin/bash

nt=32
maxfreq=$[${nt}/2]

dat=Qtop_acorr_wflow.dat
tau=(0 1 2 3 5 10)

#for i in 0 1 2 3 4 5 10 20
for i in 0 1 2 3 4 5 10 15 20
do
fig=$(basename $0)
fig=${fig#plot_}_wflow${i}
subdat=tmp-acorr-mode

if [ -f ${subdat} ]; then
  echo "exit: need to remove ${subdat}"
  exit 10
fi

for j in ${tau[@]}
do
  awk '{if ($1 == '${i}' && $2 == '${j}') {print $3, $4, $7, $8}}' ${dat} \
  >> ${subdat}
  #awk '{if ($1 == '${i}' && $2 == '${j}') {print $3, $4, $9, $10}}' ${dat} \
  #>> ${subdat}
done

gnuplot << EOF
set terminal epslatex size 12cm, 9cm color colortext font 10
set output '${fig}.tex'

#stats '${dat}'

#set offsets 0,0,0,0.1
set size 1.0,0.75

#set xlabel '\$n\$'
#set xlabel '\$4\sin^2(k/2)\$'
set xlabel '\$k^2\$'
#set xrange [-0.5:${maxfreq}]
#set xrange [-0.1:4.2]
set xrange [-0.1:4.1]
#set xtics 5
set mxtics 5
set ylabel '\$\ln \widehat{C}(k,\tau)/\widehat{C}(0,\tau) \$'
#set yrange [-1.35:1.15]
#set ytics -1,0.2,1
set mytics 5
#set key top right horizontal width 1.0 spacing 1.5 samplen 2.0 box
#set key bottom left horizontal maxcol 3 width 1.5 spacing 1.2 samplen 2.0
set key horizontal maxcol 3 width 1.5 spacing 1.2 samplen 2.0 box \
        at graph 0.7, graph -0.25

set label '\$\text{flow time}=${i}\$' at graph 0.65,0.94
#set arrow from graph 0, first 0 to graph 1, first 0 nohead 

lnum(x) = ${nt}*x
rnum(x) = ${nt}*x+${maxfreq}-1
#measure(x) = 2-2*cos(x)
measure(x) = x*x
f(x) = (x<=0) ? 1/0 : log(x)
ef(x,err) = (x<=0) ? 1/0 : err/x
#f(x) = log(abs(x))
#ef(x,err) = err/abs(x)

plot \
  '${subdat}' every ::(lnum(0))::(rnum(0)) using (measure(\$2)):(f(\$3)):(ef(\$3,\$4)) with errorbar pt 5 ps 1.5 lt 1 lw 2.0 lc rgb '#0000ff' title '\$\tau=${tau[0]}\$',\
  '${subdat}' every ::(lnum(1))::(rnum(1)) using (measure(\$2)):(f(\$3)):(ef(\$3,\$4)) with errorbar pt 6 ps 2.0 lt 1 lw 2.5 lc rgb '#0000ff' title '\$\tau=${tau[1]}\$',\
  '${subdat}' every ::(lnum(2))::(rnum(2)) using (measure(\$2)):(f(\$3)):(ef(\$3,\$4)) with errorbar pt 3 ps 2.0 lt 1 lw 2.5 lc rgb '#000000' title '\$\tau=${tau[2]}\$',\
  '${subdat}' every ::(lnum(3))::(rnum(3)) using (measure(\$2)):(f(\$3)):(ef(\$3,\$4)) with errorbar pt 4 ps 2.0 lt 1 lw 2.5 lc rgb '#00cc00' title '\$\tau=${tau[3]}\$',\
  '${subdat}' every ::(lnum(4))::(rnum(4)) using (measure(\$2)):(f(\$3)):(ef(\$3,\$4)) with errorbar pt 12 ps 2.0 lt 1 lw 2.5 lc rgb '#31a8bd' title '\$\tau=${tau[4]}\$',\
  '${subdat}' every ::(lnum(5))::(rnum(5)) using (measure(\$2)):(f(\$3)):(ef(\$3,\$4)) with errorbar pt 8 ps 2.0 lt 1 lw 2.5 lc rgb '#ff0000' title '\$\tau=${tau[5]}\$'
EOF
#plot \
#  '${subdat}' every ::(lnum(0))::(rnum(0)) using (measure(\$2)):3:4 with errorbar pt 5 ps 1.5 lt 1 lw 2.0 lc rgb '#0000ff' title '\$\tau=${tau[0]}\$',\
#  '${subdat}' every ::(lnum(1))::(rnum(1)) using (measure(\$2)):3:4 with errorbar pt 6 ps 2.0 lt 1 lw 2.5 lc rgb '#0000ff' title '\$\tau=${tau[1]}\$',\
#  '${subdat}' every ::(lnum(2))::(rnum(2)) using (measure(\$2)):3:4 with errorbar pt 3 ps 2.0 lt 1 lw 2.5 lc rgb '#000000' title '\$\tau=${tau[2]}\$',\
#  '${subdat}' every ::(lnum(3))::(rnum(3)) using (measure(\$2)):3:4 with errorbar pt 4 ps 2.0 lt 1 lw 2.5 lc rgb '#00cc00' title '\$\tau=${tau[3]}\$',\
#  '${subdat}' every ::(lnum(4))::(rnum(4)) using (measure(\$2)):3:4 with errorbar pt 12 ps 2.0 lt 1 lw 2.5 lc rgb '#31a8bd' title '\$\tau=${tau[4]}\$',\
#  '${subdat}' every ::(lnum(5))::(rnum(5)) using (measure(\$2)):3:4 with errorbar pt 8 ps 2.0 lt 1 lw 2.5 lc rgb '#ff0000' title '\$\tau=${tau[5]}\$'

combine=combine-epslatex
#${combine} -b -f bera -s large ${fig}
${combine} -f cmbright -s normalsize ${fig}
ps2pdf ${fig}.eps
#pdfcrop --margins -3 ${fig}.pdf ${fig}.pdf
pdfcrop ${fig}.pdf ${fig}.pdf
rm -f ${fig}.eps
rm -f ${subdat}

done
